# README #

This repo is created to find new antimicrobial resistance gene variants. 

## Getting started ##

	git clone https://bitbucket.org/deaytan/variantfinder/src/master/

## How it works? ##

This pipeline aims to detect the novel variants of the known antimicrobial resistance genes.
This requires first finding the contigs containing the targeted gene such as blaSHV. Next, these contigs were exported and the known gene variants (reference gene variants) were added into this multi fasta file. 
To find the SNP distances between contigs, these contigs were aligned against each other by MAFFT.
Based on the multi sequence alignments, SNP distances were calculated using snp-dist. The snp distance matrix was parsed using a custom Python script. 
This pipeline reports the variants different than the known gene variants, if they have detected among the isolates more than one time. 

This pipeline will create an output file called ${gene}_novel_variants.txt which includes the novel variants for the selected gene.
This output file includes the following information: 

* Novel variant ID
* Number of the same variants detected in the collection 
* Name of the closest reference(s) to the novel variant
* Nucleotide distance of the novel variant to the reference variant(s)
* Amino acid distance of the novel variant to the reference variant(s)

## Usage ##

	./variant_pipeline.sh -g gene_name -o /path/to/output -r /path/to/resfinder/results -d /path/to/resfinder/database

-g: the targeted gene name

-o: path to the output 

-r: path to the resfinder results

-d: path to the merged resfinder database, which is available in the repository

## Example for computerome2 ##

	./variant_pipeline.sh -g blaSHV -o /home/projects/cge/people/deaytan/TWIW/results/VariantFinderOut -r /home/projects/cge/data/projects/9000/1209/analyses/ResFinder/run_as_other_20211218 -d ./resfinderDB.fsa

## Required programs ##

* anaconda3/4.4.0
* Mafft/7.490 
* snp-dist/0.7.0 