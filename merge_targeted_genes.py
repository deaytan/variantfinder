import numpy as np
import os
import sys
import collections
import dna_to_aa

path = sys.argv[1] ##path to the resfinder results
gene = sys.argv[2] ##name the targeted gene
outname = sys.argv[3] ##name the output file
ref = sys.argv[4]

files = os.listdir(path) ##all the files we have in the database

ref_open = open(ref, "r")
ref_reads = ref_open.read()

dictm = collections.defaultdict(list)

out = open(outname, "w") ##open an output file for DNA sequences
out2 = open(outname.replace(".fna", ".faa"), "w") ##open an output  file for AA sequences

for i in files: 
	if "ResFinder_Hit_in_genome_seq.fsa" in os.listdir("%s/%s" % (path, i)):
		fopen = open("%s/%s/ResFinder_Hit_in_genome_seq.fsa" % (path, i))
		flines = fopen.read()

		parcels = flines.split(">")[1:] ##parcel the resfinder results

		for parcel in parcels: ##for each contig
			if gene in parcel: ##find the contigs inclusing the targeted gene
				parcel2 = parcel.split("\n")
				title = parcel2[0].split(",")[0]
				merged = "".join(parcel2[1:]).lower()

				if merged not in dictm[i]:

					dictm[i].append(merged)

					##DNA sequences
					out.write(">%s_%s\n" % (title, i))
					out.write(merged)
					out.write("\n")

					##AA sequences
					out2.write(">%s_%s\n" % (title, i))
					out2.write(dna_to_aa.translate(merged.replace("-","").upper()))
					out2.write("\n")


		fopen.close()


##pick the specific genes from the resfinder database 

##DNA and AA sequences
contigs = ref_reads.split(">")

for i in contigs[1:]:
	if gene in i:

		merged_seq = "".join(i.upper().split("\n")[1:])
		translated_seq = dna_to_aa.translate(merged_seq)

		##DNA sequences
		out.write(">")
		out.write(i.split("\n")[0])
		out.write("\n")
		out.write(merged_seq)
		out.write("\n")


		##AA sequences
		out2.write(">")
		out2.write(i.split("\n")[0])
		out2.write("\n")
		out2.write(translated_seq)
		out2.write("\n")
		
out.close()
out2.close()









