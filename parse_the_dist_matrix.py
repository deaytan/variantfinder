##the aim of this script is parsing the distance matrixes to find out the closest samples

import numpy as np
import sys
import collections

distmat_name = sys.argv[1]
distmat_name2 = sys.argv[2]
outname = sys.argv[3]

##distance matrix for DNA
distmat = np.loadtxt(distmat_name, dtype = "str", delimiter = "\t")

##distance matrix for AA
distmat2 = np.loadtxt(distmat_name2, dtype = "str", delimiter = "\t")

##output file
out = open(outname, "w")

out.write("variant_id\tnumber_of_variants\tclosest_reference_variant(s)\tnuc_distance_to_the_reference\taa_distance_to_the_reference\n")

##put the results together into the dict to parse better
distdict = collections.defaultdict(list)

##names of the samples 
y_axis = distmat[0][1:]
y_axis2 = list(distmat2[0][1:])

##find the reference contigs, they do not include DTU initials. This might be modified in the future. 
ref = []
ref_ind = []
for each in range(len(y_axis)):
	if "DTU" not in y_axis[each]:
		ref.append(y_axis[each])
		ref_ind.append(each + 1)

for l in range(1,len(y_axis)+1):

	##distances per sample
	dists = distmat[l][1:]

	##distances per reference
	dists_ref = distmat[l][ref_ind]

	dists[l-1] = 999
	
	##find min distances 
	min_dist = list(np.where(dists == min(dists))[0])
	
	##dist to the closest reference
	min_dist_ref = list(np.where(dists_ref == min(dists_ref))[0])

	##corresponding samples 
	min_dist_ids = y_axis[min_dist]

	##corresponding references
	min_dist_ids_ref = np.array(ref)[min_dist_ref]

	##number of aa changes compared to the closest references
	InSample = y_axis2.index(y_axis[l-1])
	refaa = []
	for element in min_dist_ids_ref:
		InRef = y_axis2.index(element)
		refaa.append(str(distmat2[InSample+1][InRef+1]))

	##collect info into a list
	distdict[y_axis[l-1]] = [list(min_dist_ids), min(dists), list(min_dist_ids_ref), min(dists_ref), refaa]

##report the variations that are different than the known variations and repeats more than one time among isolates
removed = [] ##to avoid repeating info due to the distance matrix
for each in y_axis:
	if each not in removed and distdict[each][1] == "0":
		values = distdict[each][0] ##values hold under the key
		values.append(each) ##add the key  of the dict
		if len(list(set(values) & set(ref))) == 0: ##there should not be any overlap between the reference gene variants
			out.write(each)
			out.write("\t")
			out.write(str(len(values)))
			out.write("\t")
			out.write(";".join(distdict[each][2]))
			out.write("\t")
			out.write(str(distdict[each][3]))
			out.write("\t")
			out.write(";".join(distdict[each][4]))
			out.write("\n")
		for sample in distdict[each][0]:
			removed.append(sample)

out.close()











	

	


