#!/bin/bash

module load tools anaconda3/4.4.0 mafft/7.490 snp-dists/0.7.0    

##program usage

##variant_pipeline.sh -g gene_name -o /path/to/output -r /path/to/resfinder/results -d /path/to/resfinder/database

##ResFinder database is available in the bitbucket repo

while getopts "g:o:r:d:" arg; do
	case $arg in
		g) gene=$OPTARG;;
		o) outpath=$OPTARG;;
		r) resfinder_res=$OPTARG;;
		d) resfinder_db=$OPTARG;;
	esac
done

##merge contigs including the targeted genes as long as they are unique 

echo "contigs with targeted genes are finding..."

python3 merge_targeted_genes.py $resfinder_res $gene ${outpath}/${gene}_merged.fna $resfinder_db

##alignmnet

echo "alignment is happening for DNA sequences..."

mafft --auto ${outpath}/${gene}_merged.fna > ${outpath}/${gene}_merged_dna.msa

echo "alignment is happening for amino acid sequences..."

mafft --auto ${outpath}/${gene}_merged.faa > ${outpath}/${gene}_merged_aa.msa

##distances

echo "snp distances are calculating for DNA..."

snp-dists -a ${outpath}/${gene}_merged_dna.msa > ${outpath}/${gene}_merged_dna.dist

echo "snp distances are calculating for amino acids..."

snp-dists -a ${outpath}/${gene}_merged_aa.msa > ${outpath}/${gene}_merged_aa.dist

##parse the distances

echo "novel variations are finding..."

python3 parse_the_dist_matrix.py ${outpath}/${gene}_merged_dna.dist ${outpath}/${gene}_merged_aa.dist ${outpath}/${gene}_novel_variants.txt

